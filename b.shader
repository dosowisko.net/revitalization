shader_type spatial;
//render_mode unshaded;

float hash(vec2 p) {
  return fract(sin(dot(p * 17.17, vec2(14.91, 67.31))) * 4791.9511);
}

float noise(vec2 x) {
  vec2 p = floor(x);
  vec2 f = fract(x);
  f = f * f * (3.0 - 2.0 * f);
  vec2 a = vec2(1.0, 0.0);
  return mix(mix(hash(p + a.yy), hash(p + a.xy), f.x),
         mix(hash(p + a.yx), hash(p + a.xx), f.x), f.y);
}

float fbm(vec2 x) {
  float height = 0.0;
  float amplitude = 0.5;
  float frequency = 3.0;
  for (int i = 0; i < 6; i++){
    height += noise(x * frequency) * amplitude;
    amplitude *= 0.5;
    frequency *= 2.0;
  }
  return height;
}

void vertex() {
  VERTEX.y += 0.25 + noise(VERTEX.xz * (128.0 + TIME / 128.0)) * 0.5;
  VERTEX.xz *= 0.9;
}

void fragment() {
  vec4 color = textureLod(SCREEN_TEXTURE, SCREEN_UV, 0.5);
  ALBEDO.r = mix(color.r, 0.1, 0.1);
  ALBEDO.g = mix(color.g, 0.5, 0.1);
  ALBEDO.b = mix(color.b, 1.0, 0.1);
}