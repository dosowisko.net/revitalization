extends VehicleBody

const STEER_SPEED = 2.5
const STEER_LIMIT = 0.75

var steer_target = 0

onready var accelerate_action = self.get_parent().accelerate_action
onready var reverse_action = self.get_parent().reverse_action
onready var turn_left_action = self.get_parent().turn_left_action
onready var turn_right_action = self.get_parent().turn_right_action

export var engine_force_value = 250

func _physics_process(delta):
	var fwd_mps = transform.basis.xform_inv(linear_velocity).length()
	
	steer_target = Input.get_action_strength(turn_left_action) - Input.get_action_strength(turn_right_action)
	steer_target *= STEER_LIMIT
	
	if Input.is_action_pressed(accelerate_action):
		engine_force = engine_force_value
	else:
		engine_force = 0
	
	if fwd_mps <= 5:
		engine_force *= 2
	if fwd_mps >= 15:
		engine_force /= 2
	if fwd_mps >= 25:
		engine_force /= 4
		
	if Input.is_action_pressed(reverse_action):
		if fwd_mps <= 5:
			engine_force = -engine_force_value
		else:
			if fwd_mps > 15:
				brake = 33.3
			else:
				brake = 75.0
	else:
		brake = 0.0
			
	steering = move_toward(steering, steer_target, STEER_SPEED * delta)
	if (steer_target == 0.0):
		steering = 0.0

func _ready():
	self.add_to_group("car")
