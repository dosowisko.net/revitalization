extends RigidBody

signal knocked_down
signal player_updated

onready var area = $MassCenter/Sphere/KnockDownDetector
onready var visibilityNotifier = $MassCenter/VisibilityNotifier
onready var sphere = $MassCenter/Sphere
onready var cylinder = $MassCenter/Cylinder

var player = null
var alive = true
var pending_knockdown = false
var timeout = null

func set_debug_color(what):
	return

	var material = what.mesh.get_material().duplicate()
	if self.player:
		material.albedo_color = self.player.get_parent().color
	else:
		material.albedo_color = Color(1, 1, 1, 1)
	what.material_override = material

func on_timeout():
	print(self, " WHAHAHAHA")
	#breakpoint
	self.on_knocked_down(null)

func on_knocked_down(_body):
	area.disconnect('body_entered', self, 'on_knocked_down')
	
	if not self.player and not self.pending_knockdown:
		self.timeout = get_tree().create_timer(0.5)
		self.timeout.connect("timeout", self, 'on_timeout')
		self.pending_knockdown = true
		return
		
	if not self.alive:
		return

	self.set_debug_color(cylinder)
	self.alive = false
		
	self.emit_signal("knocked_down", self, self.player)
	
	if not visibilityNotifier.is_on_screen():
		self.queue_free()
	else:
		visibilityNotifier.connect('screen_exited', self, 'queue_free')

func set_player(p):
	if not p:
		return
		
	if self.timeout:
		self.timeout.disconnect("timeout", self, "on_timeout")
		self.timeout = null
		
	self.player = p
	self.emit_signal("player_updated", self, self.player)
	self.set_debug_color(sphere)
	
	if self.pending_knockdown:
		self.on_knocked_down(null)

func on_collision(body):
	if body.is_in_group("car"):
		var force = body.linear_velocity / Vector3(32.0, 1.0, 32.0)
		force.y = max(0, force.y) + 0.01
		force.y *= rand_range(1.0, 2.0) # for comical effect
		self.apply_central_impulse(force)
		self.set_player(body)
		if self.pending_knockdown:
			self.on_knocked_down(null)
	elif body.is_in_group("tree"):
		# only update the player if the other tree is faster
		if not self.player or body.linear_velocity.length() > self.linear_velocity.length() * 1.5:
			if body.player:
				self.set_player(body.player)
			else:
				# collisions aren't reported in order, so it may happen that
				# the tree that knocked us down doesn't have its player set
				# yet, but will get it soon afterwards in another callback
				body.connect("player_updated", self, 'set_player', [], CONNECT_ONESHOT)

func _ready():
	area.connect('body_entered', self, 'on_knocked_down')
	self.connect("body_entered", self, 'on_collision')
	
	self.add_to_group("tree")
	
	var material = sphere.mesh.get_material().duplicate()
	material.albedo_color = material.albedo_color.lightened(rand_range(-0.1, 0.1))
	sphere.material_override = material

	material = cylinder.mesh.get_material().duplicate()
	material.albedo_color = material.albedo_color.lightened(rand_range(-0.025, 0.025))
	cylinder.material_override = material
