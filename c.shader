shader_type spatial;

void fragment() {
  ALBEDO.r = 0.2;
  ALBEDO.g = 0.7;
  ALBEDO.b = 0.2;
}