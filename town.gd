extends Spatial

const NUMBER_OF_TREES = 512

onready var Car1 = self.find_node("CarBase").get_child(0)
onready var Car2 = self.find_node("CarBase2").get_child(0)

onready var ScoreRed = self.find_node("ScoreRed")
onready var ScoreWhite = self.find_node("ScoreWhite")
onready var ScoreGreen = self.find_node("ScoreGreen")

var score_red = 0
var score_white = 0
var score_green = 0

func update_scores():
	ScoreRed.size_flags_stretch_ratio = score_red
	ScoreWhite.size_flags_stretch_ratio = score_white
	ScoreGreen.size_flags_stretch_ratio = score_green

func scored(_tree, car):
	if car == Car2:
		print("red scored")
		score_red += 1
	elif car == Car1:
		print("green scored")
		score_green += 1
	else:
		print("??? ", car)
		#breakpoint
	score_white -= 1
	
	self.update_scores()

func _ready():
	var tree_scene = preload("res://Tree.tscn")
	for _i in range(NUMBER_OF_TREES):
		var tree = tree_scene.instance()
		while not (abs(tree.translation.x) > 1 and abs(tree.translation.z) > 1):
			tree.translation.x = rand_range(-64, 64)
			tree.translation.z = rand_range(-64, 64)
			tree.scale.x = rand_range(0.9, 1.1)
			tree.scale.y = rand_range(0.9, 1.1)
			tree.scale.z = rand_range(0.9, 1.1)
		tree.connect("knocked_down", self, 'scored')
		add_child(tree)
		score_white += 1
	update_scores()
