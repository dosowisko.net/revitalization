shader_type spatial;
render_mode unshaded;

void fragment() {
  ALBEDO.r = 0.2;
  ALBEDO.g = 0.5;
  ALBEDO.b = 1.0;
}