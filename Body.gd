extends MeshInstance

func _ready():
	var material = self.mesh.surface_get_material(0).duplicate()
	material.albedo_color = self.get_parent().get_parent().color
	self.set_surface_material(0, material)
