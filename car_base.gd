extends Spatial

export var color : Color
export var accelerate_action = "accelerate"
export var reverse_action = "reverse"
export var turn_left_action = "turn_left"
export var turn_right_action = "turn_right"
